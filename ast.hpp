/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CPPCMS_TMPL_CC2_AST_HPP
#define CPPCMS_TMPL_CC2_AST_HPP

#include <string>
#include <vector>

#include <boost/spirit/home/x3/support/ast/position_tagged.hpp>
#include <boost/spirit/home/x3/support/ast/variant.hpp>

namespace x3 = boost::spirit::x3;


namespace cppcms_tmpl_cc2 {
namespace ast {

/*
- (x)html
- c++

Top Level Blocks (http://cppcms.com/wikipp/en/page/cppcms_1x_templates_block):
- skin
  - view
    - template

Rendering Commands (http://cppcms.com/wikipp/en/page/cppcms_1x_templates_comm):
- text
- variable
- (n)gt
- url
- include
- form
- csrf
- render
- using

Flow Control (http://cppcms.com/wikipp/en/page/cppcms_1x_templates_flow):
- if
  - elif
  - else
- foreach
  - separator
  - item
  - empty
- cache
  - trigger
 */

#define CPPCMS_TMPL_CC2_MAKE_VARIANT(name, ...) \
    struct name \
        : x3::variant<__VA_ARGS__> \
    { \
        name() = default; \
        name(const name&) = default; \
        name(name&&) = default; \
        ~name() = default; \
        using base_type::base_type; \
        name& operator=(const name&) = default; \
        name& operator=(name&&) = default; \
        using base_type::operator=; \
    }

struct cpp
    : x3::position_tagged
{
    std::string line;

    cpp() = default;

    explicit
    cpp(std::string line_)
        : line{std::move(line_)}
    {}
};

struct html
    : x3::position_tagged
{
    bool x;

    html() = default;

    explicit
    html(bool x_)
        : x{x_}
    {}
};

struct variable
    : x3::position_tagged
{
    struct filter
    {
        bool ext;
        std::string f;
        std::string args;
    };

    std::string v;
    std::vector<filter> filters;

    variable() = default;

    explicit
    variable(std::string v_,
             std::vector<filter> filters_ = {})
        : v{std::move(v_)}
        , filters{std::move(filters_)}
    {}
};

struct template_
    : x3::position_tagged
{
    struct parameter
    {
        std::string identifier;
        std::string name;
        bool const_;
        bool ref;
    };

    std::string name;
    std::vector<parameter> parameters;
    bool pure;

    template_() = default;

    template_(std::string name_,
              std::vector<parameter> parameters_ = {},
              bool pure_ = false)
        : name{std::move(name_)}
        , parameters{std::move(parameters_)}
        , pure{pure_}
    {}
};

struct view
    : x3::position_tagged
{
    std::string name;
    std::string uses;
    std::string extends;
    bool abstract;
    bool inline_;
    std::vector<template_> templates;

    view() = default;

    view(std::string name_,
         std::string uses_,
         std::string extends_ = "",
         bool abstract_ = false,
         bool inline__ = false,
         std::vector<template_> templates_ = {})
        : name{std::move(name_)}
        , uses{std::move(uses_)}
        , extends{std::move(extends_)}
        , abstract{abstract_}
        , inline_{inline__}
        , templates{std::move(templates_)}
    {}
};

struct skin
    : x3::position_tagged
{
    std::string name;
    std::vector<view> views;

    skin() = default;

    skin(std::string name_,
         std::vector<view> views_ = {})
        : name{std::move(name_)}
        , views{std::move(views_)}
    {}
};

struct document
    : x3::position_tagged
{
    CPPCMS_TMPL_CC2_MAKE_VARIANT(
        element,
        cpp,
        html,
        skin
    );

    std::vector<element> elements;

    document() = default;

    explicit
    document(std::vector<element> elements_)
        : elements{std::move(elements_)}
    {}
};

#undef CPPCMS_TMPL_CC2_MAKE_VARIANT


} // namespace ast
} // namespace cppcms_tmpl_cc2

#endif // CPPCMS_TMPL_CC2_AST_HPP
