# Copyright 2017 Adam Mizerski <adam@mizerski.pl>
#
# This file is part of cppcms_tmpl_cc2.
#
# cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.


CC := gcc
CXX := g++
CPPFLAGS :=
CXXFLAGS := -std=c++14 -O2 -Wall -Wextra
LDFLAGS :=
LDLIBS := -lstdc++ -lboost_system
OBJS := \
	cppcms_tmpl_cc2.o \
	parser.o
EXE := cppcms_tmpl_cc2

all: $(EXE)
$(EXE): $(OBJS)


DEPS := $(OBJS:.o=.d)

$(DEPS): %.d: %.cpp
	@set -e; rm -f $@; \
	 DIR=`dirname $<`; \
	 $(CXX) -MM $(CXXFLAGS) $(CPPFLAGS) $< | \
	 sed "s,^\(.*\)\.o:,$$DIR/\1.d $$DIR/\1.o:," > $@

ifneq ($(MAKECMDGOALS),clean)
-include $(DEPS)
endif


clean:
	$(RM) $(EXE) $(OBJS) $(DEPS)

.PHONY: all clean
