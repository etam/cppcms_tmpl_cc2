/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CPPCMS_TMPL_CC2_CONFIG_HPP
#define CPPCMS_TMPL_CC2_CONFIG_HPP

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/utility/error_reporting.hpp>


namespace x3 = boost::spirit::x3;


namespace cppcms_tmpl_cc2 {
namespace parser {


using iterator_type = std::string::const_iterator;
using phrase_context_type = x3::phrase_parse_context<x3::ascii::space_type>::type;
using error_handler_type = x3::error_handler<iterator_type>;

using context_type = x3::with_context<
    x3::error_handler_tag,
    const std::reference_wrapper<error_handler_type>,
    phrase_context_type
>::type;


} // namespace parser
} // namespace cppcms_tmpl_cc2

#endif // CPPCMS_TMPL_CC2_CONFIG_HPP
