/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include <string>

#include "../ast_adapted.hpp"

#include "ast_eq.hpp"
#include "ast_out.hpp"
#include "common.hpp"


CPPCMS_TMPL_CC2_TEST_PARSER(cpp)


BOOST_AUTO_TEST_SUITE( cpp )

BOOST_AUTO_TEST_CASE( correct )
{
    // given
    const auto cpp_text = std::string{"#include <foo/bar.hpp>"};
    
    const auto text = std::string{"<% c++ " + cpp_text + "%>"};
    const auto expected_ast = cppcms_tmpl_cc2::ast::cpp{cpp_text};

    // when
    const auto ast = parse(text);

    // then
    BOOST_TEST(ast == expected_ast);
}

BOOST_AUTO_TEST_SUITE_END()
