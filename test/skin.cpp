/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include <string>

#include "../ast_adapted.hpp"

#include "ast_eq.hpp"
#include "ast_out.hpp"
#include "common.hpp"


CPPCMS_TMPL_CC2_TEST_PARSER(skin)


BOOST_AUTO_TEST_SUITE( skin )

BOOST_AUTO_TEST_CASE( correct )
{
    // given
    const auto text = std::string{"<% skin %><% end %>"};
    const auto expected_ast = cppcms_tmpl_cc2::ast::skin{};

    // when
    const auto ast = parse(text);

    // then
    BOOST_TEST(ast == expected_ast);
}

BOOST_AUTO_TEST_CASE( with_name_and_end_correct )
{
    // given
    const auto name = std::string{"name"};
    
    const auto text = std::string{"<% skin " + name + " %><% end skin %>"};
    const auto expected_ast = cppcms_tmpl_cc2::ast::skin{name};

    // when
    const auto ast = parse(text);

    // then
    BOOST_TEST(ast == expected_ast);
}

BOOST_AUTO_TEST_CASE( with_view )
{
    // given
    const auto view_name = std::string{"view_name"};
    const auto view_uses = std::string{"view_uses"};

    const auto text = std::string{"<% skin %><% view "+view_name+" uses "+view_uses+" %><% end %><% end %>"};
    const auto expected_ast = cppcms_tmpl_cc2::ast::skin{
        "",
        {{view_name, view_uses}}
    };

    // when
    const auto ast = parse(text);

    // then
    BOOST_TEST(ast == expected_ast);
}

BOOST_AUTO_TEST_SUITE_END()
