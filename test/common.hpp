/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CPPCMS_TMPL_CC2_TEST_COMMON_HPP
#define CPPCMS_TMPL_CC2_TEST_COMMON_HPP

#include <string>
#include <iostream>

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/utility/error_reporting.hpp>

#include "../config.hpp"


namespace x3 = boost::spirit::x3;


namespace cppcms_tmpl_cc2 {
namespace test {


std::string someContext(const std::string::const_iterator iter,
                        const std::string::const_iterator end,
                        const std::ptrdiff_t len = 30);


template <typename Parser>
typename Parser::attribute_type parseUsing(const Parser& parserObj, const std::string& text)
{
    auto iter = text.begin();
    const auto end = text.end();
    typename Parser::attribute_type ast;

    auto error_handler = parser::error_handler_type{iter, end, std::cerr};

    const bool r = x3::phrase_parse(
        iter, end,
        x3::with<x3::error_handler_tag>(std::ref(error_handler))[parserObj],
        x3::ascii::space,
        ast);

    if (iter != end) {
        throw std::runtime_error{
            "parser didn't got to the end;\n"
            "stopped at: \"" + someContext(iter, end) + '"'};
    }
    if (!r) {
        throw std::runtime_error{"parser returned false"};
    }

    return ast;
}


} // namespace test
} // namespace cppcms_tmpl_cc2


#define CPPCMS_TMPL_CC2_TEST_PARSER(name)                               \
    namespace cppcms_tmpl_cc2 {                                         \
    namespace parser {                                                  \
                                                                        \
    using name##_type = x3::rule<struct name##_class, ast::name>;       \
    BOOST_SPIRIT_DECLARE(name##_type)                                   \
    extern const name##_type name;                                      \
                                                                        \
    }                                                                   \
    }                                                                   \
                                                                        \
    namespace {                                                         \
                                                                        \
    cppcms_tmpl_cc2::ast::name parse(const std::string& text)           \
    {                                                                   \
        return cppcms_tmpl_cc2::test::parseUsing(                       \
            cppcms_tmpl_cc2::parser::name, text                         \
        );                                                              \
    }                                                                   \
                                                                        \
    }



#endif // CPPCMS_TMPL_CC2_TEST_COMMON_HPP
