/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "common.hpp"

#include <iterator>
#include <string>


namespace cppcms_tmpl_cc2 {
namespace test {


std::string someContext(const std::string::const_iterator iter,
                        const std::string::const_iterator end,
                        const std::ptrdiff_t len)
{
    return (len < std::distance(iter, end))
            ? std::string{iter, iter+len} + "..."
            : std::string{iter, end};
}


} // namespace cppcms_tmpl_cc2
} // namespace test
