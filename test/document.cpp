/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <stdexcept>
#include <string>

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/utility/error_reporting.hpp>

#include "../ast_adapted.hpp"
#include "../config.hpp"
#include "../parser.hpp"

#include "ast_eq.hpp"
#include "ast_out.hpp"
#include "common.hpp"

namespace x3 = boost::spirit::x3;


namespace {

cppcms_tmpl_cc2::ast::document parse(const std::string& text)
{
    auto iter = text.begin();
    const auto end = text.end();
    cppcms_tmpl_cc2::ast::document ast;

    const bool r = cppcms_tmpl_cc2::parser::parse(iter, end, ast, std::cerr);

    if (iter != end) {
        throw std::runtime_error{
            "parser didn't got to the end;\n"
            "stopped at: \"" + cppcms_tmpl_cc2::test::someContext(iter, end) + '"'};
    }
    if (!r) {
        throw std::runtime_error{"parser returned false"};
    }

    return ast;
}

} // namespace


BOOST_AUTO_TEST_SUITE( document )

BOOST_AUTO_TEST_CASE( minimal_correct )
{
    namespace cms = cppcms_tmpl_cc2;
    
    // given
    const auto text = std::string{
        "<% html %>"
        "<% c++ cpp %>"
        "<% skin skin %><% end %>"
    };
    const auto expected_ast = cms::ast::document{{
        cms::ast::document::element{cms::ast::html{}},
        cms::ast::document::element{cms::ast::cpp{"cpp "}},
        cms::ast::document::element{cms::ast::skin{"skin"}},
    }};

    // when
    const auto ast = parse(text);

    // then
    BOOST_TEST(ast == expected_ast);
}

BOOST_AUTO_TEST_SUITE_END()
