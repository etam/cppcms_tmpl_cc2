/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ast_eq.hpp"

#include <algorithm>

#include "../ast.hpp"

namespace cppcms_tmpl_cc2 {
namespace ast {


bool operator==(const /*cppcms_tmpl_cc2::ast::*/cpp& l,
                const /*cppcms_tmpl_cc2::ast::*/cpp& r)
{
    return l.line == r.line;
}

bool operator==(const /*cppcms_tmpl_cc2::ast::*/html& l,
                const /*cppcms_tmpl_cc2::ast::*/html& r)
{
    return l.x == r.x;
}

bool operator==(const /*cppcms_tmpl_cc2::ast::*/variable& l,
                const /*cppcms_tmpl_cc2::ast::*/variable& r)
{
    return l.v == r.v;
}

bool operator==(const /*cppcms_tmpl_cc2::ast::*/template_::parameter& l,
                const /*cppcms_tmpl_cc2::ast::*/template_::parameter& r)
{
    return
        l.identifier == r.identifier &&
        l.name == r.name &&
        l.const_ == r.const_ &&
        l.ref == r.ref;
}

bool operator==(const /*cppcms_tmpl_cc2::ast::*/template_& l,
                const /*cppcms_tmpl_cc2::ast::*/template_& r)
{
    return
        l.name == r.name &&
        l.parameters == r.parameters &&
        l.pure == r.pure;
}

bool operator==(const /*cppcms_tmpl_cc2::ast::*/view& l,
                const /*cppcms_tmpl_cc2::ast::*/view& r)
{
    return
        l.name == r.name &&
        l.uses == r.uses &&
        l.extends == r.extends &&
        l.abstract == r.abstract &&
        l.inline_ == r.inline_
        ;
}

bool operator==(const /*cppcms_tmpl_cc2::ast::*/skin& l,
                const /*cppcms_tmpl_cc2::ast::*/skin& r)
{
    return
        l.name == r.name &&
        l.views == r.views;
}

bool operator==(const /*cppcms_tmpl_cc2::ast::*/document::element& l,
                const /*cppcms_tmpl_cc2::ast::*/document::element& r)
{
    return l.get() == r.get();
}

bool operator==(const /*cppcms_tmpl_cc2::ast::*/document& l,
                const /*cppcms_tmpl_cc2::ast::*/document& r)
{
    return l.elements == r.elements;
}

} // namespace ast
} // namespace cppcms_tmpl_cc2
