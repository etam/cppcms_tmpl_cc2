/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CPPCMS_TMPL_CC2_TEST_AST_EQ_HPP
#define CPPCMS_TMPL_CC2_TEST_AST_EQ_HPP

namespace cppcms_tmpl_cc2 {
namespace ast {

struct cpp;
struct html;
struct variable;
struct template_;
struct view;
struct skin;
struct document;


bool operator==(const /*cppcms_tmpl_cc2::ast::*/cpp&, const /*cppcms_tmpl_cc2::ast::*/cpp&);
bool operator==(const /*cppcms_tmpl_cc2::ast::*/html&, const /*cppcms_tmpl_cc2::ast::*/html&);
bool operator==(const /*cppcms_tmpl_cc2::ast::*/variable&, const /*cppcms_tmpl_cc2::ast::*/variable&);
bool operator==(const /*cppcms_tmpl_cc2::ast::*/template_&, const /*cppcms_tmpl_cc2::ast::*/template_&);
bool operator==(const /*cppcms_tmpl_cc2::ast::*/view&, const /*cppcms_tmpl_cc2::ast::*/view&);
bool operator==(const /*cppcms_tmpl_cc2::ast::*/skin&, const /*cppcms_tmpl_cc2::ast::*/skin&);
bool operator==(const /*cppcms_tmpl_cc2::ast::*/document&, const /*cppcms_tmpl_cc2::ast::*/document&);

} // namespace ast
} // namespace cppcms_tmpl_cc2

#endif // CPPCMS_TMPL_CC2_TEST_AST_EQ_HPP
