/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ast_out.hpp"

#include <ostream>

#include "../ast.hpp"

namespace cppcms_tmpl_cc2 {
namespace ast {

std::ostream& operator<<(std::ostream& o, const /*cppcms_tmpl_cc2::ast::*/cpp& cpp)
{
    return o << "<% c++ " << cpp.line << " %>";
}

std::ostream& operator<<(std::ostream& o, const /*cppcms_tmpl_cc2::ast::*/html& html)
{
    return o << "<% " << (html.x ? "x" : "") << "html %>";
}

std::ostream& operator<<(std::ostream& o, const /*cppcms_tmpl_cc2::ast::*/variable& variable)
{
    return o << "<%= " << variable.v << " %>";
}

std::ostream& operator<<(std::ostream& o, const /*cppcms_tmpl_cc2::ast::*/template_& template_)
{
    o << "<% template " << template_.name << '(';
    for (auto it = template_.parameters.begin(), end = template_.parameters.end(); it != end; ++it) {
        o << it->identifier << ' '
          << (it->const_ ? "const " : "")
          << (it->ref ? "&" : "")
          << it -> name
          << (it+1 != end ? ", " : "");
    }
    o << ')'
      << (template_.pure ? "=0" : "")
      << " %>"
      << "<% end template %>";
    return o;
}

std::ostream& operator<<(std::ostream& o, const /*cppcms_tmpl_cc2::ast::*/view& view)
{
    o << "<% view " << view.name
      << " uses " << view.uses
      << (view.extends.empty() ? "" : " extends ") << view.extends
      << (view.abstract ? " abstract" : "")
      << (view.inline_ ? " inline" : "")
      << " %>";
    for (const auto& template_ : view.templates) {
        o << template_;
    }
    o << "<% end view %>";
    return o;
}

std::ostream& operator<<(std::ostream& o, const /*cppcms_tmpl_cc2::ast::*/skin& skin)
{
    o << "<% skin" << (skin.name.empty() ? "" : " ") << skin.name << " %>";
    for (const auto& view : skin.views) {
        o << view;
    }
    o << "<% end skin %>";
    return o;
}

std::ostream& operator<<(std::ostream& o, const /*cppcms_tmpl_cc2::ast::*/document::element& element)
{
    return o << element.get();
}

std::ostream& operator<<(std::ostream& o, const /*cppcms_tmpl_cc2::ast::*/document& document)
{
    for (const auto& element : document.elements) {
        o << element << '\n';
    }
    return o;
}

} // namespace ast
} // namespace cppcms_tmpl_cc2
