/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/test/unit_test.hpp>

#include <sstream>
#include <string>
#include <vector>

#include "../ast_adapted.hpp"

#include "ast_eq.hpp"
#include "ast_out.hpp"
#include "common.hpp"


CPPCMS_TMPL_CC2_TEST_PARSER(view)


BOOST_AUTO_TEST_SUITE( view )

BOOST_AUTO_TEST_CASE( correct )
{
    // given
    const auto name = std::string{"view"};
    const auto uses = std::string{"ns::uses"};

    auto expected_asts = std::vector<cppcms_tmpl_cc2::ast::view>{};
    for (const auto& extends : {"", "extends"}) {
        for (bool abstract : {true, false}) {
            for (bool inline_ : {true, false}) {
                expected_asts.emplace_back(name, uses, extends, abstract, inline_);
            }
        }
    }

    for (const auto& expected_ast : expected_asts) {
        // given
        const auto text = [&] {
            auto strm = std::stringstream{};
            strm << expected_ast;
            return strm.str();
        }();

        // when
        const auto ast = parse(text);

        // then
        BOOST_TEST(ast == expected_ast);
    }
}

BOOST_AUTO_TEST_CASE( short_end_correct )
{
    // given
    const auto name = std::string{"view"};
    const auto uses = std::string{"uses"};

    const auto text = std::string{"<% view "+name+" uses "+uses+" %><% end %>"};
    const auto expected_ast = cppcms_tmpl_cc2::ast::view{name, uses};

    // when
    const auto ast = parse(text);

    // then
    BOOST_TEST(ast == expected_ast);
}

BOOST_AUTO_TEST_CASE( with_template )
{
    // given
    const auto name = std::string{"view"};
    const auto uses = std::string{"uses"};
    const auto t1_name = std::string{"t1_name"};
    const auto t2_name = std::string{"t2_name"};

    const auto text = std::string{
        "<% view "+name+" uses "+uses+" %>"
        "<% template "+t1_name+"() %><% end %>"
        "<% template "+t2_name+"() %><% end %>"
        "<% end %>"
    };
    const auto expected_ast = cppcms_tmpl_cc2::ast::view{
        name, uses, "", false, false,
        {
            {t1_name},
            {t2_name}
        }
    };

    // when
    const auto ast = parse(text);

    // then
    BOOST_TEST(ast == expected_ast);
}

BOOST_AUTO_TEST_SUITE_END()
