/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <vector>

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/utility/annotate_on_success.hpp>

#include "ast_adapted.hpp"
#include "parser.hpp"

namespace x3 = boost::spirit::x3;


int main()
{
    const auto text = std::string{
        "<% html %>"
        "<% c++ #include <foo.hpp> %>"
        "<% xhtml %>"
        "<% skin %><% end skin %>"
        "<% skin foo %><% end %>"
    };
    auto iter = text.begin();
    const auto end = text.end();

    cppcms_tmpl_cc2::ast::document document;

    const bool r = cppcms_tmpl_cc2::parser::parse(iter, end, document, std::cerr);

    if (r && iter == end) {
        std::cout << "success\n";

        struct printer
        {
            using result_type = void;

            int indent = 0;

            void tab() const
            {
                for (int i = 0; i < indent; ++i) {
                    std::cout << "    ";
                }
            }

            void operator()(const cppcms_tmpl_cc2::ast::cpp& cpp) const
            {
                tab();
                std::cout << "c++ '" << cpp.line << "'\n";
            }

            void operator()(const cppcms_tmpl_cc2::ast::html& html) const
            {
                tab();
                std::cout << (html.x ? "x" : "") << "html\n";
            }

            void operator()(const cppcms_tmpl_cc2::ast::skin& skin) const
            {
                tab();
                std::cout << "skin " << skin.name << '\n';
            }

            void operator()(const cppcms_tmpl_cc2::ast::document& document) const
            {
                tab();
                std::cout << "document:\n";
                for (const auto& element : document.elements) {
                    boost::apply_visitor(printer{indent+1}, element);
                }
            }
        } printer;

        printer(document);
    }
    else {
        const auto some = iter + 30;
        const auto context = std::string(iter, (some>end)?end:some);
        std::cout << "parsing failed\n"
                  << "stopped at: \"" << context << "...\"\n";
        return 1;
    }
}
