cppcms_tmpl_cc2
===============

This will be a drop-in replacement for cppcms_tmpl_cc tool from the [cppcms](http://cppcms.com/) project.

It's written from scratch in C++.

cppcms_tmpl_cc2 is released under the GNU GPLv3 license.

Features
--------

  - parser based on Boost Spirit X3
  - unit test suite
  - uses meson build system
      - Makefile can be used for building without tests


Motivation
----------

Adding support for new syntax, like this:

    <% gt "Hello <a href=\"{1}\">{2}</a>" using ( url "/user" using userId ), userId %>


Status
------

Parses part of the language into AST.
