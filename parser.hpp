/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CPPCMS_TMPL_CC2_PARSER_HPP
#define CPPCMS_TMPL_CC2_PARSER_HPP

#include <boost/spirit/home/x3.hpp>

#include "config.hpp"

namespace x3 = boost::spirit::x3;


namespace cppcms_tmpl_cc2 {

namespace ast {
struct document;
} // namespace ast


namespace parser {


using document_type = x3::rule<struct document_class, ast::document>;
BOOST_SPIRIT_DECLARE(document_type)
extern const document_type document;


bool parse(iterator_type& first, iterator_type last,
           ast::document& ast, std::ostream& err);


} // namespace parser
} // namespace cppcms_tmpl_cc2

#endif // CPPCMS_TMPL_CC2_PARSER_HPP
