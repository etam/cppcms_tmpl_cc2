/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "parser.hpp"

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/utility/annotate_on_success.hpp>

#include "ast_adapted.hpp"
#include "config.hpp"

namespace x3 = boost::spirit::x3;


namespace cppcms_tmpl_cc2 {
namespace parser {


#define CPPCMS_TMPL_CC2_RULES \
    cpp, \
    html, \
    variable, \
    template_, \
    view, \
    skin, \
    document


#define CPPCMS_TMPL_CC2_RULE(r, data, name) \
    using BOOST_PP_CAT(name, _type) = x3::rule<struct BOOST_PP_CAT(name, _class), ast::name>; \
    extern const BOOST_PP_CAT(name, _type) name; \
    const BOOST_PP_CAT(name, _type) name = BOOST_PP_STRINGIZE(name);

BOOST_PP_SEQ_FOR_EACH(CPPCMS_TMPL_CC2_RULE,
                      _,
                      BOOST_PP_VARIADIC_TO_SEQ(CPPCMS_TMPL_CC2_RULES))

#undef CPPCMS_TMPL_CC2_RULE


#define CPPCMS_TMPL_CC2_LOCAL_PARSER(type, name, expr) \
    typedef x3::rule<struct name##_class, type> name##_type; \
    const name##_type name = #name; \
    const auto name##_def = expr; \
    BOOST_SPIRIT_DEFINE(name)

CPPCMS_TMPL_CC2_LOCAL_PARSER(
    std::string, name,
    x3::raw[x3::lexeme[(x3::alpha | '_') > *(x3::alnum | '_')]]
)

CPPCMS_TMPL_CC2_LOCAL_PARSER(
    std::string, identifier,
    x3::raw[x3::lexeme[name % x3::string("::")]]
)

CPPCMS_TMPL_CC2_LOCAL_PARSER(
    std::string, variable_text,
    x3::raw[x3::lexeme[
        (-x3::string("*")) >>
        (name % (x3::string(".")|x3::string("->"))) >
        (-x3::string("()"))
    ]]
)

CPPCMS_TMPL_CC2_LOCAL_PARSER(
    std::string, string,
    x3::raw[x3::lexeme[x3::string("\"") > *((x3::char_|x3::string("\\\"")) - '"') > x3::string("\"")]]
)

CPPCMS_TMPL_CC2_LOCAL_PARSER(
    std::string, number,
    x3::raw[x3::lexeme[-x3::string("-") >> *x3::digit > -(x3::string(".") > *x3::digit)]]
)


const auto open_tag = x3::lit("<%");
const auto close_tag = x3::lit("%>");

const auto cpp_def = (open_tag >> "c++") > x3::lexeme[*(x3::char_ - close_tag)] > close_tag;

const auto html_def =
    open_tag >>
    [] {
        x3::symbols<ast::html> sym;
        sym.add
            ("html", ast::html{false})
            ("xhtml", ast::html{true})
        ;
        return sym;
    }() >
    close_tag;

CPPCMS_TMPL_CC2_LOCAL_PARSER(
    std::string, variable_args,
    -( x3::lit("(") > ((variable_text|string|number) % x3::string(",")) > x3::lit(")") )
)
CPPCMS_TMPL_CC2_LOCAL_PARSER(
    ast::variable::filter, variable_filter,
    x3::matches["ext"] > name > variable_args
)
const auto variable_def =
    (open_tag >> '=') >
    variable_text >
    *(x3::lit('|') > variable_filter) >
    close_tag;

CPPCMS_TMPL_CC2_LOCAL_PARSER(
    ast::template_::parameter, template_parameter,
    identifier > x3::matches["const"] > x3::matches['&'] > name
)
const auto template_open =
    (open_tag >> "template") > name > '(' >
    -(template_parameter % ',') >
    ')' > x3::matches[x3::lit('=') > '0'] > close_tag;
const auto template_close = open_tag > "end" > -x3::lit("template") > close_tag;
const auto template__def =
    template_open >
    template_close;

const auto view_open =
    (open_tag >> "view") > name >
    "uses" > identifier >
    -("extends" > name) >
    x3::matches["abstract"] >
    x3::matches["inline"] >
    close_tag;
const auto view_close = open_tag > "end" > -x3::lit("view") > close_tag;
const auto view_def =
    view_open >
    *template_ >
    view_close;

const auto skin_open = (open_tag >> "skin") > -(name - close_tag) > close_tag;
const auto skin_close = open_tag > "end" > -x3::lit("skin") > close_tag;
const auto skin_def =
    skin_open >
    *view >
    skin_close;

const auto document_def = *(cpp|html|skin);

BOOST_SPIRIT_DEFINE(CPPCMS_TMPL_CC2_RULES)

struct document_class
    : x3::annotate_on_success
{};


#define CPPCMS_TMPL_CC2_INSTANTIATE(r, data, name) \
    BOOST_SPIRIT_INSTANTIATE(BOOST_PP_CAT(name, _type), iterator_type, context_type)

BOOST_PP_SEQ_FOR_EACH(CPPCMS_TMPL_CC2_INSTANTIATE,
                      _,
                      BOOST_PP_VARIADIC_TO_SEQ(CPPCMS_TMPL_CC2_RULES))

#undef CPPCMS_TMPL_CC2_INSTANTIATE


bool parse(iterator_type& first, iterator_type last,
           ast::document& ast, std::ostream& err)
{
    auto error_handler = error_handler_type{first, last, err};

    const auto parser = x3::with<x3::error_handler_tag>(std::ref(error_handler)) [
        document
    ];

    return x3::phrase_parse(first, last,
                            parser,
                            x3::ascii::space,
                            ast);
}


} // namespace parser
} // namespace cppcms_tmpl_cc2
