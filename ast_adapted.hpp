/*
Copyright 2017 Adam Mizerski <adam@mizerski.pl>

This file is part of cppcms_tmpl_cc2.

cppcms_tmpl_cc2 is free software: you can redistribe it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cppcms_tmpl_cc2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cppcms_tmpl_cc2. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CPPCMS_TMPL_CC2_AST_ADAPTED_HPP
#define CPPCMS_TMPL_CC2_AST_ADAPTED_HPP

#include <boost/fusion/include/adapt_struct.hpp>

#include "ast.hpp"


BOOST_FUSION_ADAPT_STRUCT(
    cppcms_tmpl_cc2::ast::cpp,
    line
)

BOOST_FUSION_ADAPT_STRUCT(
    cppcms_tmpl_cc2::ast::variable::filter,
    ext,
    f,
    args
)

BOOST_FUSION_ADAPT_STRUCT(
    cppcms_tmpl_cc2::ast::variable,
    v,
    filters
)

BOOST_FUSION_ADAPT_STRUCT(
    cppcms_tmpl_cc2::ast::template_::parameter,
    identifier,
    const_,
    ref,
    name
)

BOOST_FUSION_ADAPT_STRUCT(
    cppcms_tmpl_cc2::ast::template_,
    name,
    parameters,
    pure
)

BOOST_FUSION_ADAPT_STRUCT(
    cppcms_tmpl_cc2::ast::view,
    name,
    uses,
    extends,
    abstract,
    inline_,
    templates
)

BOOST_FUSION_ADAPT_STRUCT(
    cppcms_tmpl_cc2::ast::skin,
    name,
    views
)

BOOST_FUSION_ADAPT_STRUCT(
    cppcms_tmpl_cc2::ast::document,
    elements
)


#endif // CPPCMS_TMPL_CC2_AST_ADAPTED_HPP
